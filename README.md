# Game of Life in Java

An implementation of
[Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life),
in Java, for the terminal.


## Build, test and run

    ./gradlew build

    ./gradlew test
    find -name index.html | xargs xdg-open

    ./gradlew --no-daemon run -q

When run directly, the application can detect the terminal dimensions itself.
When run via Gradle, the application can't detect the terminal dimensions
itself, but the build will detect and pass them in. When run via Gradle Daemon,
the build also can't detect the terimal settings, and a default size will be
used.

You can pass pass in dimensions yourself:

    STTY_SIZE="20 60" ./gradlew --no-daemon run -q

If terminal output is not rendered as expected, piping the output through `cat`
may resolve the issue.


## The implementation

Cells in the chosen window are randomly initialized, then 1000 generations are
rendered, with no delay between them.

The live cells are tracked as points on an infinite (`int`-sized) plane, and
there is no wrapping on window boundaries. In the first generation, all live
cells will be visible. In later generations, live cells may exist outside the
visible area.

Each new generation is rendered by seeing what changed since the last
generation, and only drawing those changes. ANSI control codes are used to jump
between the different locations that require updating.


## Example output

    ┌────────────────────────────────────────────────────────────────────┐
    │   O                   OO                  OO                       │
    │                                                                    │
    │                             O                                      │
    │                            OOO                                     │
    │  OO                       OO  O        OOO        O                │
    │ O  O  OO                      OO                 O O        OO     │
    │O      OO                 O   O O                 O O       O O     │
    │ O                            OO                   O         O      │
    │  O                     O    OO         OO                          │
    │  O                   OOO   OO          O O       O                 │
    │OOO                     O    OO        O  O      O O                │
    │      OO                      OO      O  O       O O                │
    │   OOOO  O OO             O   O O    O   O        O                 │
    │   OOO   O OO                  OO   O   O                           │
    │    O     OO               OO  O    O                               │
    │            O               OOO     OO   O                          │
    │    O     O OO               O        OO  O                         │
    └────────────────────────────────────────────────────────────────────┘


## To do

Things I could do, but probably won't.

### Refactoring

* Add tests for BBox and Terminal
* Use parallel streams for creating the next generation
* Do terminal initialisation by painting marks, rather than printing directly.
  By doing this, `Terminal.ansiGo` could be moved into `Advancers`, and it
  would be easier to write an integration test that uses `Terminal` and
  includes borders

### Functionality

* Do toroidal wrapping on the window boundaries
* Load paterns from files in the
  [Life 1.05 format (`*.LIF`)](http://psoup.math.wisc.edu/mcell/ca_files_formats.html#Life 1.05).
  This would require a way to recenter patterns or the window
* Export a result to a file
* Show status information (e.g. generation number, number of live cells)
* Allow user control of the run: number of generations, delay between
  generation, run until stability or a loop is reached, handle control-C
  gracefully

### Project structure & tooling

* Check test coverage
* Do some source code linting
* Add documentation comments
* Do some performance profiling
* Package the application as a JAR

