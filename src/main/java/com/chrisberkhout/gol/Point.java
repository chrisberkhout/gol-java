package com.chrisberkhout.gol;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Point {

  private int x;
  private int y;

  public Point(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public int x() {
    return x;
  }

  public int y() {
    return y;
  }

  public String toString() {
    return "Point("+x()+","+y()+")";
  }

  public boolean equals(Object other) {
    if (other == null || getClass() != other.getClass()) {
      return false;
    } else {
      Point otherPoint = (Point) other;
      return x == otherPoint.x() && y == otherPoint.y();
    }
  }

  public int hashCode() {
    return Objects.hash(x, y);
  }

  public Point shift(int xOffset, int yOffset) {
    return new Point(x + xOffset, y + yOffset);
  }

  public Set<Point> adjacent() {
    Set<Point> set = new HashSet<>();
    set.add(shift(-1,-1)); set.add(shift(0,-1)); set.add(shift(1,-1));
    set.add(shift(-1, 0));                       set.add(shift(1, 0));
    set.add(shift(-1, 1)); set.add(shift(0, 1)); set.add(shift(1, 1));
    return set;
  }

}
