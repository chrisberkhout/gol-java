package com.chrisberkhout.gol;

import java.util.Objects;
import java.util.Optional;

public class Mark implements Comparable<Mark> {
  private Point location;
  private String content;

  public Mark(Point location, String content) {
    this.location = location;
    this.content = content;
  }

  public Point location() {
    return location;
  }

  public int x() {
    return location.x();
  }

  public int maxX() {
    return location.x() + content.length() - 1;
  }

  public int y() {
    return location.y();
  }

  public String content() {
    return content;
  }

  public String toString() {
    return "Mark("+"Point("+x()+","+y()+"), \""+content+"\")";
  }

  public boolean equals(Object other) {
    if (other == null || getClass() != other.getClass()) {
      return false;
    } else {
      Mark otherMark = (Mark) other;
      return location.equals(otherMark.location()) && content.equals(otherMark.content());
    }
  }

  public int hashCode() {
    return Objects.hash(location.hashCode(), content);
  }

  public int compareTo(Mark other) {
    int comparison;
    if ((comparison = Integer.compare(y(), other.y())) != 0) { return comparison; }
    else if ((comparison = Integer.compare(x(), other.x())) != 0) { return comparison; }
    else { return Integer.compare(content.length(), other.content().length()); }
  }

  public Optional<Mark> trimTo(BBox bbox) {
    if (y() >= bbox.min().y() && y() <= bbox.max().y() &&
        x() <= bbox.max().x() && maxX() >= bbox.min().x()) {

      int overlapLeft = Math.max(x(), bbox.min().x());
      int overlapRight = Math.min(maxX(), bbox.max().x());

      int offsetLeft = overlapLeft - x();
      int offsetRightExclusive = overlapRight - x() + 1;

      return Optional.of(new Mark(new Point(overlapLeft, y()), content.substring(offsetLeft, offsetRightExclusive)));

    } else {
      return Optional.empty();
    }
  }

  public Mark trimBefore(Point point) {
    if (point.y() == y() && point.x() > x()) {
      int toTrim = Math.min(point.x() - x(), content().length());
      return new Mark(point, content().substring(toTrim));
    } else {
      return this;
    }
  }

}
