package com.chrisberkhout.gol;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.function.Supplier;

class TerminalDimensions {

  public TerminalDimensions() {}

  private Optional<Supplier<String>> getEnvSttySizeStub = Optional.empty();
  private Optional<Supplier<String>> runSttySizeStub = Optional.empty();

  public TerminalDimensions(String getEnvSttySizeResult, String runSttySizeResult) {
    getEnvSttySizeStub = Optional.of(() -> getEnvSttySizeResult);
    runSttySizeStub = Optional.of(() -> runSttySizeResult);
  }

  public static final int MIN_DIMENSION_SIZE = 4;

  public BBox get() {
    return fromSttySize(getEnvSttySize()).orElseGet(() ->
      fromSttySize(runSttySize()).orElseGet(() ->
        new BBox(new Point(1, 1), new Point(80, 25))
      )
    );
  }

  private Optional<BBox> fromSttySize(String sttySize) {
    List<Integer> nums = Optional.ofNullable(sttySize).
      map(s -> Arrays.stream(s.split(" "))).orElse(Arrays.stream(new String[0])).
      map(Integer::parseInt).
      collect(Collectors.toList());

    if (nums.size() == 2 && nums.stream().allMatch(i -> i >= MIN_DIMENSION_SIZE)) {
      return Optional.of(new BBox(new Point(1, 1), new Point(nums.get(1), nums.get(0))));
    } else {
      return Optional.empty();
    }
  }

  private String getEnvSttySize() {
    return getEnvSttySizeStub.orElse(() ->
      System.getenv("STTY_SIZE")
    ).get();
  }

  private String runSttySize() {
    return runSttySizeStub.orElse(() -> {
      String maybeOutput = null;
      try {
        Process proc = new ProcessBuilder("stty", "size").redirectInput(ProcessBuilder.Redirect.INHERIT).start();
        maybeOutput = new BufferedReader(new InputStreamReader(proc.getInputStream())).readLine();
      } catch (IOException ex) {}
      return maybeOutput;
    }).get();
  }

}
