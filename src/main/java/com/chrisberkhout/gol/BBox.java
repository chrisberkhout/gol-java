package com.chrisberkhout.gol;

import java.util.Objects;
import java.util.function.Consumer;

public class BBox {
  private Point min;
  private Point max;

  public BBox(Point min, Point max) {
    this.min = min;
    this.max = max;
  }

  public Point min() { return min; }
  public Point max() { return max; }

  public void foreach(Consumer<Point> consumer) {
    for (int y = min.y(); y <= max.y(); y++) {
      for (int x = min.x(); x <= max.x(); x++) {
        consumer.accept(new Point(x, y));
      }
    }
  }

  public String toString() {
    return "BBox("+min()+","+max()+")";
  }

  public boolean equals(Object other) {
    if (other == null || getClass() != other.getClass()) {
      return false;
    } else {
      BBox otherBBox = (BBox) other;
      return min.equals(otherBBox.min()) && max.equals(otherBBox.max());
    }
  }

  public int hashCode() {
    return Objects.hash(min.hashCode(), max.hashCode());
  }
}
