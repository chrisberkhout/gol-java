package com.chrisberkhout.gol;

import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class Painter {

  private BBox dims;
  private Advancer advancer;

  public Painter(BBox dims, Advancer advancer) {
    this.dims = dims;
    this.advancer = advancer;
  }

  public Painter(BBox dims) {
    this(dims, Advancers.whitespace);
  }

  public String paint(Set<Mark> marks) {
    StringBuilder builder = new StringBuilder();
    SortedSet<Mark> relevantMarks = marks.stream().map(mark -> mark.trimTo(dims))
      .filter(Optional::isPresent).map(Optional::get)
      .collect(Collectors.toCollection(() -> new TreeSet<>()));

    Point cursor = dims.min();
    for (Mark wholeMark : relevantMarks) {
      Mark mark = wholeMark.trimBefore(cursor);
      cursor = advancer.go(builder, dims, cursor, mark.location());
      builder.append(mark.content());
      cursor = cursor.shift(mark.content().length(), 0);
    }

    advancer.go(builder, dims, cursor, dims.max().shift(1,0));
    return builder.toString();
  }

}
