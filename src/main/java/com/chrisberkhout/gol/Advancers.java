package com.chrisberkhout.gol;

@FunctionalInterface
interface Advancer {
  Point go(StringBuilder builder, BBox dims, Point cursor, Point target);
}

class Advancers {

  public static Advancer whitespace = (StringBuilder builder, BBox dims, Point cursor, Point target) -> {
    while (target.y() > cursor.y()) {
      int numSpaces = Math.max(0, dims.max().x() - cursor.x() + 1);
      String padding = new String(new char[numSpaces]).replace("\0", " ") + "\n";
      builder.append(padding);
      cursor = new Point(dims.min().x(), cursor.y() + 1);
    }
    if (target.x() > cursor.x()) {
      String padding = new String(new char[target.x() - cursor.x()]).replace("\0", " ");
      builder.append(padding);
    }
    return target;
  };

  public static Advancer controlCodes = (StringBuilder builder, BBox dims, Point cursor, Point target) -> {
    if (target != cursor) {
      builder.append(Terminal.ansiGoto(target.x(), target.y()));
    }
    return target;
  };

}
