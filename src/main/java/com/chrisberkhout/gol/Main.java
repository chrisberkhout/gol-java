package com.chrisberkhout.gol;

class Main {
  public static void main(String[] args) {
    Terminal t = new Terminal();
    t.init();

    Step step = new Step(t.view());

    for (int i = 0; i < 1000; i++) {
      t.render(step);
      step = step.next();
    }

    t.reset();
  }
}
