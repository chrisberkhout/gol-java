package com.chrisberkhout.gol;

import java.io.PrintStream;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Terminal {

  private final String ANSI_CLS = "\u001b[2J";
  private final String ANSI_HOME = "\u001b[H";
  private final String ANSI_HIDE_CURSOR = "\u001b[?25l";
  private final String ANSI_SHOW_CURSOR = "\u001b[?25h";

  private final String BOX_DR = "┌";
  private final String BOX_DL = "┐";
  private final String BOX_UL = "┘";
  private final String BOX_UR = "└";
  private final String BOX_V = "│";
  private final String BOX_H = "─";

  public static String ansiGoto(int x, int y) {
    return "\u001b["+y+";"+x+"H";
  }
  private String gotoStatusLine() {
    return ansiGoto(dims.min().x(), dims.max().y());
  }

  private BBox dims;
  private PrintStream out;
  private Painter painter;

  public Terminal() {
    this(System.out, new TerminalDimensions().get());
  }

  public Terminal(PrintStream output, BBox dimensions) {
    dims = dimensions;
    out = output;
    painter = new Painter(view(), Advancers.controlCodes);
  }

  public BBox view() {
    return new BBox(new Point(2,2), new Point(dims.max().x() - 1, dims.max().y() - 2));
  }

  public void init() {
    out.print(ANSI_HIDE_CURSOR + ANSI_CLS + ANSI_HOME);

    out.print(BOX_DR);
    for(int i = dims.min().x() + 1; i < dims.max().x(); i++) {
      out.print(BOX_H);
    }
    out.print(BOX_DL);

    for(int j = dims.min().y() + 1; j < dims.max().y() - 1; j++) {
      out.print(ansiGoto(dims.min().x(), j) + BOX_V + ansiGoto(dims.max().x(), j) + BOX_V);
    }

    out.print(ansiGoto(dims.min().x(), dims.max().y() - 1) + BOX_UR);
    for(int i = dims.min().x() + 1; i < dims.max().x(); i++) {
      out.print(BOX_H);
    }
    out.print(BOX_UL);

    out.print(gotoStatusLine());
    out.flush();
  }

  public void render(Step step) {
    Set<Mark> marks = new HashSet();
    marks.addAll(step.newLiveCells().stream().map(p -> new Mark(p, "O")).collect(Collectors.toSet()));
    marks.addAll(step.newDeadCells().stream().map(p -> new Mark(p, " ")).collect(Collectors.toSet()));

    out.print(painter.paint(marks));
    out.flush();
  }

  public void reset() {
    out.println(ansiGoto(dims.max().x(), dims.max().y())+ANSI_SHOW_CURSOR);
  }

}
