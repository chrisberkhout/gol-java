package com.chrisberkhout.gol;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.BooleanSupplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Step {
  private BBox view;
  private Set<Point> liveCells;
  private Set<Point> previouslyLiveCells;

  public Step(BBox view) {
    this(view, () -> ThreadLocalRandom.current().nextBoolean());
  }

  public Step(BBox view, BooleanSupplier toPopulate) {
    this(view, new HashSet<>());
    view.foreach(point -> {
      if (toPopulate.getAsBoolean()) {
        liveCells.add(point);
      }
    });
  }

  public Step(String population) {
    String[] rows = population.split("\n");

    view = new BBox(
      new Point(1,1),
      new Point(Stream.of(rows).map(row -> (row.length())).max(Integer::max).orElse(0), rows.length)
    );
    liveCells = new HashSet<>();

    view.foreach(point -> {
      int i = point.x() - view.min().x();
      int j = point.y() - view.min().y();
      if (j < rows.length && i < rows[j].length()) {
        if (rows[j].charAt(i) == 'O') {
          liveCells.add(point);
        }
      }
    });
  }

  private Step(BBox view, Set<Point> liveCells) {
    this(view, liveCells, new HashSet<>());
  }

  private Step(BBox view, Set<Point> liveCells, Set<Point> previouslyLiveCells) {
    this.view = view;
    this.liveCells = liveCells;
    this.previouslyLiveCells = previouslyLiveCells;
  }

  public BBox view() {
    return view;
  }

  public Set<Point> liveCells() {
    return liveCells;
  }

  public Set<Point> newLiveCells() {
    Set<Point> newLive = new HashSet(liveCells);
    newLive.removeAll(previouslyLiveCells);
    return newLive;
  }

  public Set<Point> newDeadCells() {
    Set<Point> newDead = new HashSet(previouslyLiveCells);
    newDead.removeAll(liveCells);
    return newDead;
  }

  public String toString() {
    Set<Mark> marks = liveCells.stream().map(p -> new Mark(p, "O")).collect(Collectors.toSet());
    return new Painter(view, Advancers.whitespace).paint(marks);
  }

  public Step next() {
    Set<Point> candidatePoints = new HashSet<>();
    for (Point cell : liveCells) {
      candidatePoints.addAll(cell.adjacent());
    }

    Set<Point> nextLiveCells = new HashSet<>();
    for (Point point : candidatePoints) {
      int n = liveNeighboursOf(point);
      if (n == 3 || (n == 2 && liveCells.contains(point))) {
        nextLiveCells.add(point);
      }
    }

    return new Step(view, nextLiveCells, liveCells);
  }

  private int liveNeighboursOf(Point point) {
    Set<Point> neighbours = point.adjacent();
    neighbours.retainAll(liveCells);
    return neighbours.size();
  }

}
