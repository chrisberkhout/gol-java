package com.chrisberkhout.gol;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AdvancersTest {

  public static class WhitespaceTest {

    Advancer subject = Advancers.whitespace;
    BBox dims = new BBox(new Point(1,1), new Point(10,5));

    @Test
    public void forwardsPrintsSpaces() {
      StringBuilder sb = new StringBuilder();
      Point at = new Point(2,2);
      Point to = new Point(6,2);
      assertEquals(to, subject.go(sb, dims, at, to));
      assertEquals("    ", sb.toString());
    }

    @Test
    public void cursorAtTargetDoesNothing() {
      StringBuilder sb = new StringBuilder();
      Point at = new Point(2,2);
      Point to = at;
      assertEquals(to, subject.go(sb, dims, at, to));
      assertEquals("", sb.toString());
    }

    @Test
    public void downWrapsWithNewline() {
      StringBuilder sb = new StringBuilder();
      Point at = new Point(8,1);
      Point to = new Point(2,2);
      assertEquals(to, subject.go(sb, dims, at, to));
      assertEquals("   \n ", sb.toString());
    }

    @Test
    public void padsFullLinesIfNecessary() {
      StringBuilder sb = new StringBuilder();
      Point at = new Point(6,1);
      Point to = new Point(6,4);
      assertEquals(to, subject.go(sb, dims, at, to));
      String expected = String.join("\n",
             "     ",
        "          ",
        "          ",
        "     ");
      assertEquals(expected, sb.toString());
    }

    @Test
    public void backwardsDoesNothing() {
      StringBuilder sb = new StringBuilder();
      Point at = new Point(2,2);
      Point to = new Point(1,1);
      assertEquals(to, subject.go(sb, dims, at, to));
      assertEquals("", sb.toString());
    }

    @Test
    public void fromPastEOLStillWrapsCorrectly() {
      StringBuilder sb = new StringBuilder();
      Point at = new Point(15,1);
      Point to = new Point(5,2);
      assertEquals(to, subject.go(sb, dims, at, to));
      assertEquals("\n    ", sb.toString());
    }

  }

  public static class ControlCodesTest {

    Advancer subject = Advancers.controlCodes;
    BBox dims = new BBox(new Point(1,1), new Point(10,5));

    @Test
    public void forwardsJumps() {
      StringBuilder sb = new StringBuilder();
      Point at = new Point(3,2);
      Point to = new Point(7,4);
      assertEquals(to, subject.go(sb, dims, at, to));
      assertEquals("\u001b["+to.y()+";"+to.x()+"H", sb.toString());
    }

    @Test
    public void backwardsJumps() {
      StringBuilder sb = new StringBuilder();
      Point at = new Point(7,4);
      Point to = new Point(3,2);
      assertEquals(to, subject.go(sb, dims, at, to));
      assertEquals("\u001b["+to.y()+";"+to.x()+"H", sb.toString());
    }

    @Test
    public void cursorAtTargetDoesNothing() {
      StringBuilder sb = new StringBuilder();
      Point at = new Point(3,2);
      Point to = at;
      assertEquals(to, subject.go(sb, dims, at, to));
      assertEquals("", sb.toString());
    }

  }

}
