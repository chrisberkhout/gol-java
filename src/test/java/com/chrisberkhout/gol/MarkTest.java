package com.chrisberkhout.gol;

import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MarkTest {

  @Test
  public void areEqualIfTheyHaveTheSameValues() {
    assertTrue(new Mark(new Point(1,2), "X").equals(new Mark(new Point(1,2), "X")));
  }

  @Test
  public void haveMatchingHashCodesIfTheyHaveTheSameValues() {
    assertEquals(new Mark(new Point(1,2), "X").hashCode(), new Mark(new Point(1,2), "X").hashCode());
  }

  @Test
  public void orderedByYAscendingThenXAscendingThenContentLengthAscending() {
    Mark upperLeft = new Mark(new Point(-1,1), "X");
    Mark upperRight = new Mark(new Point(2,1), "X");
    Mark lowerLeft = new Mark(new Point(1,2), "X");
    Mark longerLowerLeft = new Mark(new Point(1,2), "XXX");

    assertTrue(upperRight.compareTo(lowerLeft) < 0);
    assertTrue(upperLeft.compareTo(upperRight) < 0);
    assertTrue(longerLowerLeft.compareTo(lowerLeft) > 0);
  }

  @Test
  public void canBeTrimmedToBoundingBoxWhenEnclosed() {
    Mark original = new Mark(new Point(2,2), "X");
    Mark trimmed = original;
    assertEquals(Optional.of(trimmed), original.trimTo(new BBox(new Point(1,1), new Point(3,3))));
  }

  @Test
  public void canBeTrimmedToBoundingBoxWhenOverflowingRight() {
    Mark original = new Mark(new Point(2,2), "ABCD");
    Mark trimmed = new Mark(new Point(2,2), "AB");
    assertEquals(Optional.of(trimmed), original.trimTo(new BBox(new Point(1,1), new Point(3,3))));
  }

  @Test
  public void canBeTrimmedToBoundingBoxWhenOverflowingLeft() {
    Mark original = new Mark(new Point(-1,2), "ABCD");
    Mark trimmed = new Mark(new Point(1,2), "CD");
    assertEquals(Optional.of(trimmed), original.trimTo(new BBox(new Point(1,1), new Point(3,3))));
  }

  @Test
  public void canBeTrimmedToBoundingBoxWhenOverflowingRightAndLeft() {
    Mark original = new Mark(new Point(0,2), "ABCDE");
    Mark trimmed = new Mark(new Point(1,2), "BCD");
    assertEquals(Optional.of(trimmed), original.trimTo(new BBox(new Point(1,1), new Point(3,3))));
  }

  @Test
  public void willBeTrimmedOffWhenRight() {
    Mark original = new Mark(new Point(4,2), "ABCD");
    assertEquals(Optional.empty(), original.trimTo(new BBox(new Point(1,1), new Point(3,3))));
  }

  @Test
  public void willBeTrimmedOffWhenLeft() {
    Mark original = new Mark(new Point(-4,2), "ABCD");
    assertEquals(Optional.empty(), original.trimTo(new BBox(new Point(1,1), new Point(3,3))));
  }

  @Test
  public void willBeTrimmedOffWhenAbove() {
    Mark original = new Mark(new Point(1,0), "ABCD");
    assertEquals(Optional.empty(), original.trimTo(new BBox(new Point(1,1), new Point(3,3))));
  }

  @Test
  public void willBeTrimmedOffWhenBelow() {
    Mark original = new Mark(new Point(1,4), "ABCD");
    assertEquals(Optional.empty(), original.trimTo(new BBox(new Point(1,1), new Point(3,3))));
  }

  @Test
  public void canTrimmedUpToAPoint() {
    Point cursor = new Point(3,1);
    Mark mark = new Mark(new Point(1,1), "ABCD");
    assertEquals(new Mark(new Point(3,1), "CD"), mark.trimBefore(cursor));
  }

}
