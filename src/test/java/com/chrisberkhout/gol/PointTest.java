package com.chrisberkhout.gol;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PointTest {

  @Test
  public void canBeCreatedWithValues() {
    new Point(3,5);
  }

  @Test
  public void areEqualIfTheyHaveTheSameValues() {
    assertTrue(new Point(3,5).equals(new Point(3,5)));
  }

  @Test
  public void haveMatchingHashCodesIfTheyHaveTheSameValues() {
    assertEquals(new Point(3,5).hashCode(), new Point(3,5).hashCode());
  }

  @Test
  public void canBeShifted() {
    Point original = new Point(3,5);
    Point shifted = original.shift(2,2);

    assertEquals(5, shifted.x());
    assertEquals(7, shifted.y());
  }

  @Test
  public void canGenerateAdjacentPoints() {
    Point point = new Point(3,5);

    Set<Point> expected = new HashSet<>();
    expected.add(new Point(2,4)); expected.add(new Point(3,4)); expected.add(new Point(4,4));
    expected.add(new Point(2,5));                               expected.add(new Point(4,5));
    expected.add(new Point(2,6)); expected.add(new Point(3,6)); expected.add(new Point(4,6));

    assertEquals(expected, point.adjacent());
  }

}
