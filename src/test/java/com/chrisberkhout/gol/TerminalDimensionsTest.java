package com.chrisberkhout.gol;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TerminalDimensionsTest {

  BBox expected = new BBox(new Point(1,1), new Point(142,39));
  BBox theDefault = new BBox(new Point(1,1), new Point(80,25));

  @Test
  public void parsesEnvCorrectly() {
    assertEquals(expected, new TerminalDimensions("39 142", null).get());
  }

  @Test
  public void runsSttySizeIfEnvIsUnset() {
    assertEquals(expected, new TerminalDimensions(null, "39 142").get());
  }

  @Test
  public void runsSttySizeIfEnvIsBad() {
    assertEquals(expected, new TerminalDimensions("123", "39 142").get());
  }

  @Test
  public void finallyFallsBackToDefault() {
    assertEquals(theDefault, new TerminalDimensions(null, null).get());
  }

  @Test
  public void invalidWhenSmallerThanMinInX() {
    assertEquals(theDefault, new TerminalDimensions("25 3", null).get());
  }

  @Test
  public void invalidWhenSmallerThanMinInY() {
    assertEquals(theDefault, new TerminalDimensions("3 80", null).get());
  }

}
