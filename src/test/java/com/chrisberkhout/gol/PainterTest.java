package com.chrisberkhout.gol;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class PainterTest {

  @Test
  public void canBeCreatedWithDimensions() {
    new Painter(new BBox(new Point(1,1), new Point(6,5)));
  }

  @Test
  public void canUseAnAlternateAdvancer() {
    BBox dims = new BBox(new Point(1,1), new Point(5,3));
    Painter painter = new Painter(dims, Advancers.controlCodes);
    Point at = new Point(2,2);
    Set<Mark> marks = new HashSet<>();
    String content = "ABC";
    marks.add(new Mark(at, content));
    Point cursorEndsAt = dims.max().shift(1,0);
    String expected = "\u001b["+at.y()+";"+at.x()+"H"+content+
                      "\u001b["+cursorEndsAt.y()+";"+cursorEndsAt.x()+"H";
    assertEquals(expected, painter.paint(marks));
  }

  @Test
  public void paintsAsStringOfCorrectDimensions() {
    Painter painter = new Painter(new BBox(new Point(1,1), new Point(6,5)));
    String expected = String.join("\n",
      "      ",
      "      ",
      "      ",
      "      ",
      "      ");
    assertEquals(expected, painter.paint(new HashSet<>()));
  }

  @Test
  public void paintsGivenMarks() {
    Painter painter = new Painter(new BBox(new Point(1,1), new Point(6,5)));
    Set<Mark> marks = new HashSet<>();
    marks.add(new Mark(new Point(1,2), "X"));
    marks.add(new Mark(new Point(3,4), "YYY"));
    String expected = String.join("\n",
      "      ",
      "X     ",
      "      ",
      "  YYY ",
      "      ");
    assertEquals(expected, painter.paint(marks));
  }

  @Test
  public void paintsOverflowingMarksCorrectly() {
    Painter painter = new Painter(new BBox(new Point(1,1), new Point(6,3)));
    Set<Mark> marks = new HashSet<>();
    marks.add(new Mark(new Point(3,2), "ABCDEF"));
    marks.add(new Mark(new Point(-1,3), "XYZ"));
    String expected = String.join("\n",
      "      ",
      "  ABCD",
      "Z     ");
    assertEquals(expected, painter.paint(marks));
  }

  @Test
  public void paintIgnoresNonIntersectingMarks() {
    Painter painter = new Painter(new BBox(new Point(1,1), new Point(6,3)));
    Set<Mark> marks = new HashSet<>();
    marks.add(new Mark(new Point( 0, 0), "XXX"));
    marks.add(new Mark(new Point( 7, 2), "XXX"));
    marks.add(new Mark(new Point( 0, 4), "XXX"));
    marks.add(new Mark(new Point(-2, 2), "XXX"));
    String expected = String.join("\n",
      "      ",
      "      ",
      "      ");
    assertEquals(expected, painter.paint(marks));
  }

  @Test
  public void paintSkipsOverlappingMarkSectionsAlreadyCovered() {
    Painter painter = new Painter(new BBox(new Point(1,1), new Point(9,3)));
    Set<Mark> marks = new HashSet<>();
    marks.add(new Mark(new Point(1,2), "ABCD"));
    marks.add(new Mark(new Point(2,2),  "JK"));
    marks.add(new Mark(new Point(4,2),    "UVW"));
    marks.add(new Mark(new Point(8,2),        "XY"));
    String expected = String.join("\n",
      "         ",
      "ABCDVW XY",
      "         ");
    assertEquals(expected, painter.paint(marks));
  }

}
