package com.chrisberkhout.gol;

import org.junit.Test;

import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class StepTest {

  @Test
  public void canBeCreatedWithDimensions() {
    Step step = new Step(new BBox(new Point(1,1), new Point(4,4)));
  }

  @Test
  public void hasAStringRepresentationWithTheRightDimensions() {
    Step step = new Step(new BBox(new Point(1,1), new Point(5,4)));

    String[] rows = step.toString().split("\n");
    assertEquals(5, rows[0].length());
    assertEquals(4, rows.length);
  }

  @Test
  public void canBeCreatedWithPopulationStringOfOddShape() {
    String population = String.join("\n",
      "      ",
      "      ",
      "  O   ",
      "      ",
      "      ");
    String oddShapedPopulation = population.substring(0, population.length() - 3);
    Step step = new Step(oddShapedPopulation);
    assertEquals(population, step.toString());
  }

  @Test
  public void handlesUnderPopulation() {
    String initial = String.join("\n",
      "      ",
      "      ",
      "  O   ",
      "      ",
      "      ");
    String next = String.join("\n",
      "      ",
      "      ",
      "      ",
      "      ",
      "      ");
    assertEquals(next, new Step(initial).next().toString());
  }

  @Test
  public void handlesSurvivalWith2() {
    String initial = String.join("\n",
      "      ",
      " O    ",
      "  O   ",
      "   O  ",
      "      ");
    String next = String.join("\n",
      "      ",
      "      ",
      "  O   ",
      "      ",
      "      ");
    assertEquals(next, new Step(initial).next().toString());
  }

  @Test
  public void handlesSurvivalWith3() {
    String initial = String.join("\n",
      "      ",
      " OO   ",
      " OO   ",
      "      ",
      "      ");
    String next = String.join("\n",
      "      ",
      " OO   ",
      " OO   ",
      "      ",
      "      ");
    assertEquals(next, new Step(initial).next().toString());
  }

  @Test
  public void handlesReproduction() {
    String initial = String.join("\n",
      "      ",
      " O O  ",
      "      ",
      "  O   ",
      "      ");
    String next = String.join("\n",
      "      ",
      "      ",
      "  O   ",
      "      ",
      "      ");
    assertEquals(next, new Step(initial).next().toString());
  }

  @Test
  public void handlesOverPopulation() {
    String initial = String.join("\n",
      "      ",
      " OO   ",
      " OO   ",
      " OO   ",
      "      ");
    String next = String.join("\n",
      "      ",
      " OO   ",
      "O  O  ",
      " OO   ",
      "      ");
    assertEquals(next, new Step(initial).next().toString());
  }

  @Test
  public void knowsWhichLiveCellsAreNew() {
    String initial = String.join("\n",
      "      ",
      " OO   ",
      " OO   ",
      " OO   ",
      "      ");
    String expectedNewLiveCells = String.join("\n",
      "      ",
      "      ",
      "O  O  ",
      "      ",
      "      ");
    Step next = new Step(initial).next();
    Set<Mark> marks = next.newLiveCells().stream().map(p -> new Mark(p, "O")).collect(Collectors.toSet());
    String knownNewLiveCells = new Painter(next.view(), Advancers.whitespace).paint(marks);
    assertEquals(expectedNewLiveCells, knownNewLiveCells);
  }

  @Test
  public void knowsWhichDeadCellsAreNew() {
    String initial = String.join("\n",
      "      ",
      " OO   ",
      " OO   ",
      " OO   ",
      "      ");
    String expectedNewDeadCells = String.join("\n",
      "      ",
      "      ",
      " XX   ",
      "      ",
      "      ");
    Step next = new Step(initial).next();
    Set<Mark> marks = next.newDeadCells().stream().map(p -> new Mark(p, "X")).collect(Collectors.toSet());
    String knownNewDeadCells = new Painter(next.view(), Advancers.whitespace).paint(marks);
    assertEquals(expectedNewDeadCells, knownNewDeadCells);
  }

}
